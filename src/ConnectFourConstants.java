
public interface ConnectFourConstants {
	public static final int PLAYER1 = 1;
	public static final int PLAYER2 = 2;
	public static final int PLAYER1_WON = 1;
	public static final int PLAYER2_WON = 2;
	public static final int DRAW = 3;
	public static final int CONTINUE = 4;
}
